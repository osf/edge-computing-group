=================
How to contribute
=================

Our community welcomes all people interested in open source cloud
computing, and encourages you to join the `Open Infrastructure Foundation
<https://openinfra.dev/join>`_.

To learn more about the working group and its activities see the wiki page for
more resources:
https://wiki.openstack.org/wiki/Edge_Computing_Group

The best way to get involved with the group is to join the mailing list and the
weekly calls:
* http://lists.openstack.org/cgi-bin/mailman/listinfo/edge-computing
* https://wiki.openstack.org/wiki/Edge_Computing_Group#Meetings

You can also reach contributors on IRC on Freenode: #edge-computing-group

We welcome all types of contributions, from sharing your edge computing use
case, help building reference architecture models or participate in testing
activities we have.

The community is collaborating through Git and Gerrit, therefore pull requests
submitted through GitHub will be ignored.
