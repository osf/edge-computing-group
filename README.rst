====================
Edge Computing Group
====================

This repository contains resources and artifacts of the OIF Edge Computing
Group.

You can find out more information about the working group on this wiki:
https://wiki.openstack.org/wiki/Edge_Computing_Group

You can reach contributors on IRC on Freenode: #edge-computing-group

Licenses
--------

In this repository all documentation is licensed under `Creative-Commons
Attribution 4.0 International License <LICENSES/CC-BY-4.0>`_ and all code
is licensed under `Apache License 2.0 <LICENSES/APACHE-2>`_  if not
indicated otherwise.
